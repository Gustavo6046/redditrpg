from .vector import Vector

import math


class TextCanvas(object):
    def __init__(self):
        self.reset_lines()
        self.pivot = (0, 0)

    def reset_lines(self):
        self.lines = [
            ' ' * 70
        for _ in range(35)]

    def set_pivot(self, x, y):
        self.pivot = (x, y)

    def render(self, cropped = False):
        return '```\n{}\n```'.format(self.render_raw(cropped))

    def render_raw(self, cropped = False):
        l = list(self.lines)

        if cropped:
            while l and not l[0].replace(' ', ''):
                l.pop(0)

            while l and not l[-1].replace(' ', ''):
                l.pop()

        res = '\n'.join(l[::-1])
        self.reset_lines()
        
        return res

    def __setitem__(self, xy, char):
        self.put(*xy, char)

    def put(self, x, y, char):
        x = int(x - self.pivot[0] + len(self.lines[0]) / 2)
        y = int(y - self.pivot[1] + len(self.lines) / 2)

        if y >= 0 and y < len(self.lines) and x >= 0 and x < len(self.lines[y]):
            self.lines[y] = self.lines[y][:x] + char + self.lines[y][x + 1:]

    def __getitem__(self, xy):
        x, y = xy
        return self.lines[y][x]


class CanvasPen(object):
    def __init__(self, target: TextCanvas):
        self.target = target
        self.pos = Vector((0, 0))
        self.alpha = 1
        self.quant_error = 0
        self.angle = 0

    def aim(self, angle):
        self.angle = angle

    def turn_right(self, angle_offset = 90):
        self.angle -= angle_offset

    def turn_left(self, angle_offset = 90):
        self.angle += angle_offset

    def forward(self, length, char = ''):
        offset = Vector((
            math.cos(self.angle) * length,
            math.sin(self.angle) * length
        ))

        if char:
            self.line_toward(offset, char)

        else:
            self.move(offset)

    def set_alpha(self, alpha):
        self.alpha = alpha
        self.quant_error = 0

    def reset_alpha(self):
        self.set_alpha(1)
    
    def move_to(self, pos):
        self.pos = Vector(pos)

    def move(self, offs):
        self.pos += Vector(offs)

    def line_toward(self, offs, char):
        self.line_to(self.pos + Vector(offs), char)

    def line_to(self, pos2, char):
        pos = self.pos
        pos2 = Vector(pos2)

        if pos.x == pos2.x and pos.y == pos2.y:
            self.put(pos.x, pos.y, char)
            return

        self.pos = pos2

        if pos.x > pos2.x:
            self.move_to(pos2)
            self.line_to(pos, char)
            self.move_to(pos2)

        offs = pos2 - pos

        if offs.y == 0:
            for x in range(int(pos.x), int(pos2.x) + 1):
                self.put(x, pos.y, char)

        elif offs.x == 0:
            y0 = int(min(pos.y, pos2.y))
            y1 = int(max(pos.y, pos2.y))

            for y in range(y0, y1 + 1):
                self.put(pos.x, y, char)
        
        else:
            slope = offs.y / offs.x
            
            if abs(slope) <= 1:
                for x in range(int(pos.x), int(pos2.x) + 1):
                    real_y = pos.y + (x - pos.x) * slope
                    y = int(real_y)
                    self.put(x, y, char)
            
            else:
                y1 = int(pos.y)
                y2 = int(pos2.y)

                for y in range(min(y1, y2), max(y1, y2) + 1):
                    real_x = pos.x + (y - pos.y) * (1 / slope)
                    x = int(real_x)
                    self.put(x, y, char)
        
    def write_size(self, text: str):
        w = 0
        h = 0

        x = 0

        for l in text: 
            if l == '\n':
                x = 0
                h += 1

            else:       
                x += 1

                if x > w:
                    w = x

        return (w, h)

    def write(self, text: str):
        x = self.pos.x
        y = self.pos.y

        self.pos += Vector((len(text), 0))

        for l in text: 
            if l == '\n':
                x = self.pos.xy
                y += 1

            else:       
                self.put(x, y, l)
                x += 1

    def arc(self, radius, angle = math.pi * 2, char = ''):
        # Adapted from Turtle code.
        arc_length = radius * abs(angle)
        num_steps = int(arc_length / 3) + 1
        step_length = arc_length / num_steps
        step_angle = float(angle) / num_steps

        self.turn_left(step_angle / 2)

        for _ in range(num_steps):
            self.forward(step_length, char)
            self.turn_left(step_angle)

        self.turn_right(step_angle / 2)

    def rect(self, size, char, pos = None):
        alpha = self.alpha
        self.alpha = 1

        pos = pos or self.pos
        
        pos = Vector(pos)
        size = Vector(size)

        if alpha > 0:
            if alpha == 1:
                for x in range(pos.x, pos.x + size.x):
                    for y in range(pos.y, pos.y + size.y):
                        self.put(x, y, char)

            else:
                values = [alpha for _ in range(size.x * size.y)]

                def add_safe(idx, val):
                    if idx < len(values):
                        values[idx] += val

                for y in range(pos.y, pos.y + size.y):
                    for x in range(pos.x, pos.x + size.x):
                        # apply Floyd-Steinberg dithering
                        index = (y - pos.y) * size.y + (x - pos.x)

                        if values[index] > 0.5:
                            self.put(x, y, char)
                            error = values[index] - 1

                        else:
                            error = values[index]

                        add_safe(index + 1         , error * 7 / 16)
                        add_safe(index - 1 + size.x, error * 3 / 16)
                        add_safe(index     + size.x, error * 5 / 16)
                        add_safe(index + 1 + size.x, error * 1 / 16)

        self.alpha = alpha

    def put(self, x, y, char):
        if self.alpha > 0:
            self.quant_error += 1

            if self.quant_error >= 1 / self.alpha:
                self.target.put(x, y, char)
                self.quant_error -= 1 / self.alpha

    def here(self, char):
        self.put(self.pos.x, self.pos.y, char)