from redditorrpg.game import Game

import sys
import time
import trio


def main():
    if len(sys.argv) > 1:
        game = Game(sys.argv[1], 'gamestate.db')

        for sub in sys.argv[2:]:
            print('Checking: ', sub)
            game.bot.add_submission(sub)

        game.play_turn()

        if game.bot.replies:
            for seconds in range(5, 0, -1):
                print('Safe reply wait: ', str(seconds) + '...')
                time.sleep(1)

            print('Replying!')
            trio.run(game.bot.send_replies)
            
        print('Done!')

    else:
        game = Game(None, 'gamestate.db')

        print('    Enter your name, please.')
        pname = input()

        if pname in game.player_index:
            p = game.player_index[pname]

        else:
            p = game.make_player(pname)

        while True:
            screen = game.render_raw(p.entity)
            print('\x1B[2J\x1B[0;0H' + screen)

            print('\n    Enter a command:\n\n\x1B[1A', end = '')
            command = input()

            if command in ('quit', 'q'):
                print('\x1B[2J\x1B[0;0HGoodbye!')
                exit(0)

            else:
                game.player_command(command, pname)
                game.play_turn()
                game.tick()

if __name__ == '__main__':
    main()