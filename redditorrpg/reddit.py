from . import players, meta
from typing import Set, Union

import traceback
import time
import praw
import typing
import trio
import string
import json


valid_commands = (
    # Movement
    'up', 'down', 'left', 'right',

    # Miscellaneous
    'hi',
)

def reddit_open(json_filename):
    with open(json_filename) as json_fp:
        data = json.loads(json_fp.read())
        
    return praw.Reddit(
        client_id = data['clientID'],
        client_secret = data['clientSecret'],
    
        user_agent = data['userAgent'].format(
            VERSION = meta.VERSION
        ),

        username = data['username'],
        password = data['password']
    )



class DummyBot(object):
    def check_submissions(self):
        pass

    def add_submission(self):
        pass

    def remove_submission(self):
        pass


class Bot(object):
    DEBUG = False
    prefix = 'RPG>'

    def __init__(self, game: 'Game', account_filename: str, last_check: float):
        self.game = game
        self.reddit = reddit_open(account_filename)
        self.submissions = set() # type: Set[praw.models.Submission]
        self.name = self.reddit.user.me().name
        self.last_check = last_check
        self.replies = []

    async def send_replies(self):
        if not self.DEBUG:
            while len(self.replies) > 0:
                obj, rep = self.replies.pop(0)

                try:
                    obj.reply(rep)

                except praw.exceptions.APIException:
                    traceback.print_exc()
                    self.replies.insert(0, (obj, rep))
                    await trio.sleep(60)

                else:
                    if len(self.replies) > 0:
                        await trio.sleep(20)

    def check_submissions(self):
        ch = time.time()

        for s in self.submissions:
            self.check_submission(s)

        self.last_check = ch

    def check_submission(self, s: praw.models.Submission):
        comm = s.comments
        comm.replace_more(limit=0)

        for c in comm.list(): # type: praw.models.Comment
            self.check_comment(c)

    def check_comment(self, c: praw.models.Comment):
        if c.author is None:
            return

        if c.created_utc > self.last_check and c.author.name != self.name:
            self.post_check_comment(c)

    def post_check_comment(self, c: praw.models.Comment):
        has_command = False
        pre = []

        for line in c.body.split('\n'):
            command = self.parse_line(line)
            
            if command:
                print(c.author.name, '|', line)
                pre.append(self.execute_command(command, c))
                has_command = True

        if has_command and c.author.name in self.game.player_index:
            player = self.game.player_index[c.author.name]

            res = self.game.render(player.entity)
            self.add_reply(c, '\n'.join(p for p in pre if p) + res)

    def add_reply(self, obj, reply):
        self.replies.append((obj, reply))

    def execute_command(self, command: str, c: praw.models.Comment):
        if command not in valid_commands:
            self.add_reply(c, '**Invalid commmand "**{}**"!** The following commands are available: _{}_'.format(command, ', '.join(valid_commands)))
            return

        pre = ''

        if c.author.name not in self.game.player_index:
            pre = '_(New player added: **{}**! Welcome!)_\n\n'.format(c.author.name)
            self.game.make_player(c.author.name)
            print('(New player: ' + c.author.name + ')')

        player = self.game.player_index[c.author.name]
        player.execute_event(command)

        return pre

    def add_submission(self, submission: typing.Union[praw.models.Submission, str]):
        if isinstance(submission, str):
            submission = self.reddit.submission(url=submission)

        self.submissions.add(submission)

    def remove_submission(self, submission: typing.Union[praw.models.Submission, str]):
        if isinstance(submission, str):
            submission = self.reddit.submission(submission)

        self.submissions.remove(submission)

    def parse_line(self, line: str) -> str:
        line = line[(line.index('#') if '#' in line else 0) :].strip(string.whitespace)

        if line == '':
            return None

        else:
            if line.upper().startswith(self.prefix.upper()):
                return line[len(self.prefix):].lstrip(string.whitespace)

            return None
    
    def parse(self, text: str) -> typing.Iterator[str]:
        return (x for x in (self.parse_line(l) for l in text.split('\n')) if x)
    
    def get_redditor(self, user: typing.Union[players.Player, str]) -> praw.models.Redditor:
        if isinstance(user, players.Player):
            user = user.name

        return self.reddit.redditor(user)

    def get_comment(self, id: str) -> praw.models.Comment:
        return self.reddit.comment(id)